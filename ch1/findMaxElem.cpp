#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

void vectorPrint(vector<int> &V);
void vectorGen(vector<int> &V, int n);
int findMaxElemI(vector<int> &V);
int findMaxElemR(vector<int> &V);

int main(int argc, char** argv)
{
    vector<int> v_test;
    vectorGen(v_test, 20);
    vectorPrint(v_test);
    cout << "Iteration Version: " << findMaxElemI(v_test) << endl;
    cout << "Recursion Version: " << findMaxElemR(v_test) << endl;
    return 0;
}

// Iteration Version
int findMaxElemI(vector<int> &V)
{
    if(V.empty())
    {
        return 0;
    }
    auto max = V[0];
    for(auto e : V)
    {
        if(e > max)
        {
            max = e;
        }
    }
    return max;
}

int findMaxR(vector<int> &V, int lo, int hi)
{
    // recursion base
    if(hi - lo == 1)
    {
        return V[lo];
    }
    int mi = (hi+lo) >> 1;
    return max(findMaxR(V, lo, mi), findMaxR(V, mi, hi));
}

int findMaxElemR(vector<int> &V)
{
    return findMaxR(V, 0, V.size());
}

void vectorGen(vector<int> &V, int n)
{
    srand(time(nullptr));
    for(int i=0; i<n; i++)
    {
        V.push_back(rand() % (3*n));
    }
}

void vectorPrint(vector<int> &V)
{
    for(auto e : V)
    {
        cout << e << " ";
    }
    cout << endl;
}