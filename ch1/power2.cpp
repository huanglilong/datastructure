#include <iostream>
#include <ctime>

using namespace std;
int64_t power2_I(int n);
int64_t power2_R(int n);
int64_t power2_I2(int n);
int64_t power_I(int a, int n);

int main(int arc, char** argv)
{
    srand(time(nullptr));
    int num = rand() % 20;
    cout << "rand num " << num << endl;
    cout << "iteration version " << power2_I(num) << endl;
    cout << "recursion version " << power2_R(num) << endl;
    cout << "iteration version 2: " << power2_I2(num) <<endl;
    cout << "Power for any base: " << power_I(3, num) << endl;
    return 0;
}

// 求解     : 2^n
// 单调性   : n--
int64_t power2_I(int n)
{
    int products = 1;
    while(n)
    {
        products <<= 1;
        n--;
    }
    return products;
}

// 计算每bit的权重
int64_t power2_I2(int n)
{
    int64_t s = 1;
    int64_t base = 2;
    while(0 < n)
    {
        if( n & 1)
        {
            s = s*base;
        }
        base *= base;
        n >>= 1;
    }
    return s;
}

// 求解     : 2^n
// 单调性   : n = n/2
int64_t power2_R(int n)
{
    if(n==0)
    {
        return 1;
    }
    int64_t s = power2_R(n>>1);
    return (n & 1) ? 2*s*s : s*s;
}

// power function for any base
int64_t power_I(int a, int n)
{
    int products = 1;
    int base = a;
    while(0 < n)
    {
        if(n & 1)
        {
            products *= base;
        }
        base *= base;
        n >>= 1;
    }
    return products;
}