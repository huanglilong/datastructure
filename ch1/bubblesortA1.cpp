#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void bubbleSortA1(int A[], int n);
void bubbleSortA2(int A[], int n);
void bubbleSortA3(int A[], int n);
void numbersGen(int A[], int n);
void arrayPrint(int A[], int n);

int main(int argc, char** argv)
{
    int A[20];
    
    // Bubble Sort Version A1 Testing
    cout << "Bubble Sort Version A1 Testing." << endl;
    numbersGen(A, 20);
    arrayPrint(A, 20);
    bubbleSortA1(A, 20);
    arrayPrint(A, 20);
    // Bubble Sort Version A2 Testing
    cout << "Bubble Sort Verion A2 Testing." << endl;
    numbersGen(A, 20);
    arrayPrint(A, 20);
    bubbleSortA2(A, 20);
    arrayPrint(A, 20);
    // Bubble Sort Version A3 Testing
    cout << "Bubble Sort Verion A3 Testing." << endl;
    numbersGen(A, 20);
    arrayPrint(A, 20);
    bubbleSortA3(A, 20);
    arrayPrint(A, 20);
    return 0;
}

// 不变性: 经过k次扫描交换, 最大前k个元素必然就位;
// 单调性: 经过k次扫描交换, 待解问题有效规模缩减至n-k;
void bubbleSortA1(int A[], int n)
{
    for(int i=0; i<n; i++)              // n趟
    {
        for(int j=1; j<n-i; j++)        // 每趟, n-i: 每趟交换都有元素就位, 算法不变性
        {
            if(A[j] < A[j-1])           // 逆序
            {
                swap(A[j], A[j-1]);     // 交换逆序对
            }
        }
        arrayPrint(A, n);               // 输出每趟扫描交换后的数组
    }
}

// 利用逆序对信息, 提前结束循环
// 一旦检测无逆序对存在, 则排序完成
void bubbleSortA2(int A[], int n)
{
    bool sorted = false;        // 排序完成标志
    while(!sorted)              // 数组存在逆序对
    {
        sorted = true;          // 假定排序前无逆序对
        for(int i=1; i<n; i++)  // 每趟扫描
        {
            if(A[i-1] > A[i])   // 存在逆序对
            {
                swap(A[i-1], A[i]);
                sorted = false;
            }
            
        }
        arrayPrint(A, n);       // 输出扫描交换后, 数组
        n--;                    // 算法不变性, 每趟扫描后, 必有元素就位
    }
}

// 利用最后逆序位置, 优化后面交换长度
void bubbleSortA3(int A[], int n)
{
    int sorted_pos = n;                 // 初始状态,认为没有元素处于有序状态
    while(sorted_pos > 0)               // 存在逆序元素
    {
        int last_pos = 0;
        for(int i=0; i<sorted_pos; i++) // [sorted_pos, n) 区间元素已就绪, 不存在逆序对
        {
            if(A[i] < A[i-1])
            {
                swap(A[i], A[i-1]);
                last_pos = i;
            }
        }
        arrayPrint(A, last_pos);        // 输出扫描交换后, 数组
        sorted_pos = last_pos;          // 记下最后逆序对位置, 此时A[last_pos]元素已就绪, 就绪区间扩大
    }
}
void numbersGen(int A[], int n)
{
    srand(time(nullptr));
    for(int i=0; i<n; i++)
    {
        A[i] = rand() % (3*n);
    }
}

void arrayPrint(int A[], int n)
{
    for(int i=0; i<n; i++)
    {
        cout << A[i] << " ";
    }
    cout << endl;
}