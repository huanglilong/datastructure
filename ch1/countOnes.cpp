#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int countOnesA1(uint64_t n);
int countOnesA2(uint64_t n);

int main(int argc, char** argv)
{
    srand(time(nullptr));
    uint64_t rand_number = rand() % 1000;
    cout << "A1, rand_number " << rand_number << "'s binary bits: " << countOnesA1(rand_number) << endl;
    cout << "A2, rand_number " << rand_number << "'s binary bits: " << countOnesA2(rand_number) << endl;
    return 0;
}

// 单调性: 每次左移一位, 问题规模减一
int countOnesA1(uint64_t n)
{
    int counts = 0;
    while(n)
    {
        counts += n & 1;    // 取 Bit 1
        n >>= 1;            // 左移一位
    }
    return counts;
}

// 单调性: 每执行一次必清掉n最后为1的bit
int countOnesA2(uint64_t n)
{
    int counts = 0;
    while(n)
    {
        counts += 1;
        n &= n-1;       // 清除n当前最后为1的bit
    }
    return counts;
}