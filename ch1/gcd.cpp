#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;
int gcd(int a, int b);

int main(int argc, char** argv)
{
    srand(time(nullptr));
    int a = rand() % 1000;
    int b = rand() % 1000;
    cout << "a is " << a << ", " << "b is " << b << endl;
    cout << "gcd: " << gcd(a,b) << endl;
    return 0;
}

int gcd(int a, int b)
{
    int result = 1;
    while(!(a & 1) && !(b & 1))    // a和b是偶数
    {
        result <<= 1;
        a >>= 1;
        b >>= 1;
    }
    while(1)                        // a, b不会同时为偶数
    {
        while(!(a & 1)) a >>=1;
        while(!(b & 1)) b >>=1;
        (a >= b) ? a = a-b : b = b-a; // gcd(a, b) = gcd(a-b, b), if(a > b)
        if(a==0) return b*result;
        if(b==0) return a*result;
    }
}