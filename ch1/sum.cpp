#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

void numbersGen(int A[], int n);
void arrayPrint(int A[], int n);
int sumI(int A[], int n);
int sumR(int A[], int n);
void reverse(int A[], int n);
void reverse(int A[], int lo, int hi);
int sum_BR(int A[], int n);

int main(int argc, char** argv)
{
    int A[10];
    numbersGen(A, 10);
    arrayPrint(A, 10);
    cout << "sumI version: " << sumI(A, 10) << endl;
    cout << "sumR version: " << sumR(A, 10) << endl;
    cout << "sumER version: " << sum_BR(A, 10) << endl;
    reverse(A, 10);
    arrayPrint(A, 10);
    return 0;
}

/* iteration version */
int sumI(int A[], int n)
{
    int sum = 0;
    for(int i=0; i<n; i++)
    {
        sum += A[i];
    }
    return sum;
}

/* recursion version */
int sumR(int A[], int n)
{
    // rerursion base
    if(1 > n)
    {
        return 0;
    }
    return sumR(A, n-1) + A[n-1]; // divide-and-conquer
}

// binary recursion
int sum_BR(int A[], int lo, int hi)
{
    if(hi - lo == 1)        // recursion base
    {
        return A[lo];
    }
    int mi = (lo + hi + 1) >> 1;
    return sum_BR(A, lo, mi) + sum_BR(A, mi, hi);
}
// binary recursion
int sum_BR(int A[], int n)
{
    sum_BR(A, 0, n);
}
void reverse(int A[], int n)
{
    reverse(A, 0, n);
}

void reverse(int A[], int lo, int hi)
{
    if(lo < hi)
    {
        swap(A[lo], A[hi-1]);
        reverse(A, lo+1, hi-1);
    }
}
/* Tool functions */
void numbersGen(int A[], int n)
{
    srand(time(nullptr));
    for(int i=0; i<n; i++)
    {
        A[i] = rand() % (3*n);
    }
}

void arrayPrint(int A[], int n)
{
    for(int i=0; i<n; i++)
    {
        cout << A[i] << " ";
    }
    cout << endl;
}