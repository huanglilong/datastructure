#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;
int64_t fibonacci_R1(int n);
int64_t fibonacci_M(int n,int64_t&);
int64_t fibonacci_DP(int n);

int main(int argc, char** argv)
{
    int64_t prev;
    srand(time(nullptr));
    int num = rand() % 20;
    cout << "rand num: " << num << endl;
    cout << "fibonacci I version: " << fibonacci_R1(num) << endl;
    cout << "fibonacci M version: " << fibonacci_M(num, prev) << endl;
    cout << "fibonacci DP version: " << fibonacci_DP(num) << endl;
    return 0;
}

int64_t fibonacci_R1(int n)
{
    // recursion base
    if(n <= 1)
    {
        return n;
    }
    return fibonacci_R1(n-1) + fibonacci_R1(n-2);
}

// memoization
int64_t fibonacci_M(int n, int64_t& prev)
{
    // recursion base
    if(n==0)
    {
        prev = 1; return 0;  // fib(-1) = 1, fib(0) = 0;
    }
    int64_t prevprev;
    prev = fibonacci_M(n-1, prevprev);
    return prevprev + prev;
}

// dynamic programming
int64_t fibonacci_DP(int n)
{
    int64_t f = 1, g = 0; // fib(1) = 1, fib(0) = 0
    while(0 < n--)
    {
        f = f + g;  // fib(k) = fib(k-1) +  fib(k-2)
        g = f - g;  // fib(k-1) = fib(k) - fib(k-2)
    }
    return g;
}