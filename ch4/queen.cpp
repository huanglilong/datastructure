#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Stack.h"

using namespace std;
void palceQueens(int N);
void placeQueensB(int N);

struct Queen
{
    int x, y;       // (x, y)
    Queen(int xx=0, int yy=0) : x(xx), y(yy) { }
    bool operator==(Queen const& q)
    {
        if((q.x==x) || (q.y==y) || (q.x-x == q.y-y) || (q.x-x == y-q.y))
        {
            return true;
        }
        return false;
    }
    bool operator!=(Queen const& q)
    {
        return !(*this == q);
    }
};

int main(int argc, char** argv)
{
    cout << "**********************************" << endl;
    cout << "Version A" << endl;
    palceQueens(4);
    cout << "**********************************" << endl;
    cout <<"Version B" << endl;
    placeQueensB(4);
    return 0;
}

void palceQueens(int N)
{
    Stack<Queen> soluStack;
    int xc=0, yr=0;                             // start position

    while((xc < N) && (yr < N))
    {
        // deal with column
        while(xc < N)                           // search every column
        {
            Queen curr_q(xc, yr);               // next column
            if(soluStack.find(curr_q) == -1)    // vaild position for next Queen
            {
                cout << "push: " << "(" << curr_q.x << "," << curr_q.y << ") " << endl;
                soluStack.push(curr_q);
                break;
            }
            xc++;      
        }
        // deal with row
        if(!(xc < N))                           // column search failed
        {
            Queen q = soluStack.pop();
            cout << "pop: " << "(" << q.x << "," << q.y << ") " << endl;
            if(q.x + 1 == N)                    // if in last column, we need try pop prev queen and try it's next position
            {
                q = soluStack.pop();
                cout << "pop: " << "(" << q.x << "," << q.y << ") " << endl;
            }
            yr = q.y;                           // try next position
            xc = q.x+1;
        }
        else
        {
            xc = 0;
            yr++;                               // for next row
        }

    }
}

/**********************************************
 * Book Version
 * ********************************************/
void placeQueensB(int N)
{
    Stack<Queen> solu;
    Queen q(0,0);
    do
    {
        if(N <= solu.size() || N <= q.y)
        {
            q = solu.pop();
            cout << "pop: " << "(" << q.x << "," << q.y << ") " << endl;
            q.y++;
        }
        else
        {
            while((q.y < N) && (0 <= solu.find(q)))
            {
                q.y++;
            }
            if(N > q.y)
            {
                solu.push(q);
                cout << "push: " << "(" << q.x << "," << q.y << ") " << endl;
                q.x++;
                q.y = 0;
            }
        }
    }while((0 < q.x) || (q.y < N));
}
