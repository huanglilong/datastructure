#include <iostream>
#include <ctime>
#include <cstdlib>

#include "Stack.h"

using std::cout;
using std::endl;

void convert(Stack<char>& S, int64_t n, int base);

int main(int argc, char** argv)
{
    srand(time(nullptr));
    int64_t num = rand() % 64;
    Stack<char> hexStack;
    cout << "rand number is " << num << endl;
    convert(hexStack, num, 16);
    cout << "0x";
    while(!hexStack.empty())
    {
        cout << hexStack.pop();
    }
    cout << endl;
    return 0;
}

void convert(Stack<char>& S, int64_t n, int base)
{
    static char digit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    while(0 < n)
    {
        S.push(digit[n % base]);
        n /= base;
    }
}