#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>
#include "Stack.h"

using namespace std;
bool parenMatch(const char exp[], int lo, int hi);

int main(int argc, char** argv)
{
    char exp[] = "1+(2+3) + (3-4)*8)";
    cout << "string size " << sizeof(exp) << endl;
    cout << parenMatch(exp, 0, sizeof(exp));
    return 0;
}

bool parenMatch(const char exp[], int lo, int hi)
{
    Stack<char> expStack;
    for(int i=lo; i<hi; i++)
    {
        if(exp[i] == '(')
        {
            expStack.push('(');
        }
        if(exp[i] == ')')
        {
            if(expStack.empty())
            {
                return false;
            }
            else
            {
                expStack.pop();
            }
        }
    }
    if(expStack.empty())
    {
        return true;
    }
    else
    {
        return false;
    }
}