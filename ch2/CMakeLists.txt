#
# Toolchain     : g++ 5.1.0
# Time          : 2018/04/09
# Author        : huanglilong<huanglilongwk@gmail.comm>
#

#
# 1. This is CMake Template, Don't using this template for library
# 2. How to use
#    (1). mkdir build                       // create a directory for build
#    (2). cd build
#    (3). cmake .. -G "Unix Makefiles"      // using cmake generate makefiles
#    (3). make                              // generate binary
#
cmake_minimum_required(VERSION 3.5)
project(project CXX)

# enable C++14 future
list(APPEND CMAKE_CXX_FLAGS "-std=c++14")

# Bulid Type -- Debug
set(CMAKE_BUILD_TYPE Debug)

# Include header file path
include_directories(
    ${PROJECT_SOURCE_DIR}
)

# Collect all source files(cpp file), using relative file path
file(GLOB CPP_SRCS
    RELATIVE ${PROJECT_SOURCE_DIR}
    "*.cpp"
)

# Generate excutable file for each cpp file
foreach(SRC ${CPP_SRCS})
    string(REPLACE ".cpp" "" SRC_BASE ${SRC})  # get source file's basename
    add_executable(${SRC_BASE} ${SRC})         # don't this tamplate for library
endforeach()
