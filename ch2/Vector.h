#pragma once

/************************************************************************
 * Vector Template Class decarlation
 * **********************************************************************/
typedef int Rank;
#define DEFAULT_CAPACITY 3  // 默认容量

template <typename T>
class Vector
{
protected:
    Rank _size; int _capacity; T* _elem;
    void copyFrom(const T* A, Rank lo, Rank hi);
    void expand();
    void shrink();
    bool bubble(Rank lo, Rank hi);
    void bubbleSort(Rank lo, Rank hi);
    Rank max(Rank lo, Rank hi);
    void selectionSort(Rank lo, Rank hi);
    void merge(Rank lo, Rank mi, Rank hi);
    void mergeSort(Rank lo, Rank hi);
    void insertSort(Rank lo, Rank hi);
    // Rank partition(Rank lo, Rank hi);
    // void quickSort(Rank lo, Rank hi);
    // void heapSort(Rank lo, Rank hi);
public:
    // constructor functions
    Vector(int c = DEFAULT_CAPACITY, int s = 0, T v = 0)
    {
        _elem = new T[_capacity=c];
        for(_size=0; _size<s; _elem[_size++]=v);
    }
    Vector(const T* A, int lo, int hi)
    {
        copyFrom(A, lo, hi);
    }
    Vector(const T* A, int n)
    {
        copyFrom(A, 0, n);
    }
    Vector(Vector<T> const& v, Rank lo, Rank hi)
    {
        copyFrom(v._elem, lo, hi);
    }
    Vector(Vector<T> const& v)
    {
        copyFrom(v._elem, 0, v._size);
    }
    // deconstructor
    ~Vector()
    {
        delete [] _elem;
    }
    // Read-only Interface
    Rank size() const { return _size; }
    bool empty() const { return _size == 0; }
    int disordered() const;
    Rank find(const T& e) const { return find(e, 0, _size); } // unorder vector search
    Rank find(const T& e, Rank lo, Rank hi) const;
    Rank search(const T& e) const
    { return (0 >= _size) ? -1 : search(e, 0, _size); }
    Rank search(const T& e, Rank lo, Rank hi) const;
    // modify interface
    T& operator[](Rank r) const;
    Vector<T> & operator=(Vector<T> const& v);
    T remove(Rank r);
    int remove(Rank lo, Rank hi);
    Rank insert(Rank r, const T& e);
    Rank insert(const T& e) { return insert(_size, e); } // insert in end
    void sort(Rank lo, Rank hi);
    void sort() { sort(0, _size); }
    void unsort(Rank lo, Rank hi);
    void unsort() { unsort(0, _size); }
    int deduplicate();
    int uniquify();
    // 遍历
    void traverse(void (*)(T&));    // using function pointer
    template <typename VST>
    void traverse(VST&);
};

/*************************************************************************
 * Vector Template Class Implement
 * ***********************************************************************/
template <typename T>
void Vector<T>::copyFrom(const T* A, Rank lo, Rank hi)
{
    _elem = new T[_capacity=(hi-lo)*2];
    _size = 0;
    while(lo < hi)
    {
        _elem[_size++] = A[lo++];
    }
}

template <typename T>
Vector<T>& Vector<T>::operator=(Vector<T> const& V)
{
    if(_elem) delete [] _elem;
    _elem = new T[_capacity=V._capacity];
    copyFrom(V._elem, 0, V._size);
    return *this;
}

template <typename T>
void Vector<T>::expand()
{
    if(_size < _capacity) return;
    if(_capacity < DEFAULT_CAPACITY) _capacity = DEFAULT_CAPACITY;
    T* oldElem = _elem;
    _elem = new T[_capacity <<= 1];
    for(int i=0; i<_size; i++) // here, don't use copyFrom function
    {
        _elem[i] = oldElem[i];
    }
    delete [] oldElem;
}

template <typename T>
void Vector<T>::shrink()
{
    if(4*_size > _capacity) return;
    T* oldElem = _elem;
    _elem = new T[_capacity >>= 1];
    for(int i=0; i < _size; i++)
    {
        _elem[i] = oldElem[i];
    }
    delete [] oldElem;
}

template <typename T>
T& Vector<T>::operator[](Rank r) const
{
    return _elem[r];
}

template <typename T>
void permute(Vector<T>& v)
{
    for(int i=v.size(); i>0; i--)
    {
        swap(v[i-1], v[rand() % i]);
    }
}

template <typename T>
void Vector<T>::unsort(Rank lo, Rank hi)
{
    T* V = _elem + lo;  // [lo, hi)
    for(Rank i=hi-lo; i>0; i--)
    {
        swap(V[i-1], V[rand() % i]);
    }
}

template <typename T>
Rank Vector<T>::find(const T& e, Rank lo, Rank hi) const
{
    while((lo < hi--) && (_elem[hi] != e));
    return hi;
}

template <typename T>
Rank Vector<T>::insert(Rank r, const T& e)
{
    expand();
    for(Rank i=_size; i > r; i--)
    {
        _elem[i] = _elem[i-1];
    }
    _elem[r] = e;
    _size++;
    return r;
}

template <typename T>
int Vector<T>::remove(Rank lo, Rank hi)
{
    while(hi < _size)
    {
        _elem[lo++] = _elem[hi++];
    }
    _size = lo;
    shrink();
    return hi-lo;
}

template <typename T>
T Vector<T>::remove(Rank r)
{
    T e = _elem[r];
    remove(r, r+1);
    return e;
}

template <typename T>
int Vector<T>::deduplicate()
{
    int oldSize = _size;
    Rank pos = 1;
    while(pos < _size)  // start at _elem[1]
    {
        (find(_elem[pos], 0, pos) == -1) ? pos++ : remove(pos, pos+1);
    }
    return oldSize - _size;
}

// operations for order vector

template <typename T>
int Vector<T>::disordered() const
{
     int counts = 0;
     for(int i=1; i < _size; i++)
     {
        if(_elem[i-1] > _elem[i])
        {
            counts += 1;
        }
     }
     return counts;
}

template <typename T>
int Vector<T>::uniquify()
{
    int oldSize = _size;
    int pos = 1;
    for(int i=1; i<_size; i++)
    {
        if(_elem[pos-1] != _elem[i])
        {
            _elem[pos++] = _elem[i];
        }
    }
    _size = pos;
    shrink();
    return oldSize - _size;
}

// template <typename T>
// static Rank binSearchA1(T* A, const T& e, Rank lo, Ranlk hi)
// {
//     while(lo < hi)  // when search space vaild
//     {
//         Rank mi = (hi + lo) >> 1;
//         if(e < A[mi]) // in [lo, mi)
//         {
//             hi = mi;
//         }
//         else if(A[mi] < e) // in [mi+1, hi)
//         {
//             lo = mi + 1;
//         }
//         else    // A[mi] = e
//         {
//             return mi;
//         }
//     }
//     return -1; // fail
// }

// template <typename T>
// static Rank Vector<T>::binSearch(T* A, const T& e, Rank lo, Rank hi)
// {
//     while(hi - lo > 1)      // space size great than 1
//     {
//         Rank mi = (hi + lo) >> 1;
//         if(A[mi] < e)       // in [mi, hi), A[mi] < e
//         {
//             lo = mi;
//         }
//         else                // in [lo, mi), e <= A[mi]
//         {
//             hi = mi;
//         }
//     }
//     return (_elem[lo] == e) ? lo : -1;
// }

template <typename T>
static Rank binSearch(T* A, const T& e, Rank lo, Rank hi)
{
    while(lo < hi)
    {
        Rank mi = (lo + hi) >> 1;
        if(e < A[mi])               // [mi, hi), e < A[mi]
        {
            hi = mi;
        }
        else                        // [0, lo), A[mi] <= e
        {
            lo = mi+1;              // e < A[lo]
        }
    }
    lo--;
    return lo;
}

template <typename T>
Rank Vector<T>::search(const T& e, Rank lo, Rank hi) const
{
    return binSearch(_elem, e, lo, hi);
}

// Sort Algorithm
template <typename T>
void Vector<T>::sort(Rank lo, Rank hi)
{
    //bubbleSort(lo, hi);
    selectionSort(lo, hi);
    //mergeSort(lo, hi);
    //insertSort(lo, hi);
    //heapSort(lo, hi);
    //quickSort(lo, hi);
}

template <typename T>
void Vector<T>::bubbleSort(Rank lo, Rank hi)
{
    while(!bubble(lo, hi--));
}

template <typename T>
bool Vector<T>::bubble(Rank lo, Rank hi)
{
    bool sorted = false;
    while(++lo < hi)
    {
        sorted = true;
        if(_elem[lo-1] > _elem[lo])
        {
            swap(_elem[lo-1], _elem[lo]);
            sorted = false;
        }
    }
    return sorted;
}

template <typename T>
void Vector<T>::mergeSort(Rank lo, Rank hi)
{
    // recursion base
    if(hi - lo < 2)     // single element case
    {
        return;
    }
    Rank mi = (lo + hi) >> 1;
    mergeSort(lo, mi);
    mergeSort(mi, hi);
    merge(lo, mi, hi);
}

template <typename T>
void Vector<T>::merge(Rank lo, Rank mi, Rank hi)
{
    int b_size = mi - lo;
    int c_size = hi - mi;
    T* B = new T[b_size];
    T* C = _elem + mi;                      // C, [mi, hi)
    T* A = _elem + lo;                      // A, [lo, hi)
    for(int i=0; i<mi; B[i] = A[i++]);      // B, [lo, mi)
    
    // merge two array
    int bi=0, ci=0, ai=0;
    while((bi<b_size) || (ci<c_size))
    {
        if((bi<b_size) && (!(ci<c_size) || (B[bi] <= C[ci])))
        {
            A[ai++] = B[bi++];
        }
        if((ci<c_size) && (!(bi<b_size) || (C[ci] < B[bi])))
        {
            A[ai++] = C[ci++];
        }
    }
    delete [] B;
}

template <typename T>
static Rank selectMax(T* A, Rank lo, Rank hi)
{
    T max = A[hi-1];
    Rank result = hi-1;
    while(lo < hi--)
    {
        if(A[hi] > max)
        {
            max = A[hi];
            result = hi;
        }
    }
    return result;
}

template <typename T>
void Vector<T>::selectionSort(Rank lo, Rank hi)
{
    while(lo < hi)
    {
        Rank max_rank = selectMax(_elem, 0, hi);
        std::swap(_elem[--hi], _elem[max_rank]);
    }
}

template <typename T>
void Vector<T>::insertSort(Rank lo, Rank hi)
{
    while(++lo < hi)
    {
        Rank insert_rank = search(_elem[lo], 0, lo);
        insert(insert_rank+1, remove(lo));
    }
}