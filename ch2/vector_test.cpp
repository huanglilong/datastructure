#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Vector.h"

using namespace std;

int main(int argc, char** argv)
{
    srand(time(nullptr));
    int num =  rand() % 10;
    Vector<int> v_test;

    // Generate Vector
    cout << "Rand Vector: " << endl;
    for(int i=0; i<10; i++)
    {
        v_test.insert(int(rand() % 10));
        cout << v_test[i] << " ";
    }
    cout << endl;
    // Sort Vector
    cout << "size: " << v_test.size() << endl;
    v_test.sort(0, v_test.size());
    cout << "Insert Sort:" << endl;
    for(int i=0; i<v_test.size(); i++)
    {
        cout << v_test[i] << " ";
    }
    cout << endl;
    // cout << "search " << num << " " << v_test.search(num, 0, v_test.size()) << endl;
    // cout << v_test.remove(4) << endl;
    // cout << v_test.size() << endl;
    return 0;
}
